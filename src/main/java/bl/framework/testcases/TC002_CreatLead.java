package bl.framework.testcases;


import org.openqa.selenium.WebElement;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import bl.framework.api.SeleniumBase;
import bl.framework.design.ProjectsMethods;

public class TC002_CreatLead extends SeleniumBase{
	@Test (dataProvider="call")
	
		public void lead(String cname, String fname, String lname ) {
		startApp("Chrome", "http://leaftaps.com/opentaps");
		WebElement elemuser = locateElement("id", "username");
		clearAndType(elemuser, "DemoSalesManager");
		WebElement elempass = locateElement("id", "password");
		clearAndType(elempass, "crmsfa");
		WebElement login = locateElement("class", "decorativeSubmit");
		click(login);
		WebElement link = locateElement("text", "CRM/SFA");
		click(link);
		WebElement creatlead = locateElement("xpath", "//a[text()='Create Lead']");
		click(creatlead);
		WebElement leadname = locateElement("id", "createLeadForm_companyName");
		clearAndType(leadname, cname);
		WebElement firstname = locateElement("id", "createLeadForm_firstName");
		clearAndType(firstname, fname);
		WebElement lastname = locateElement("id", "createLeadForm_lastName");
		clearAndType(lastname, lname);
		WebElement create = locateElement("class", "smallSubmit");
		click(create);
		
		
		
	}
	@DataProvider (name="call")
	public String[][] fetchdata() {
		String[] [] data= new String[1][3];
		data[0][0]= "TestLeaf";
		data[0][1]= "udhaya";
		data[0][2]= "s";
		return data;
	}
	@DataProvider (name="run")
	public String[][] fetchdata1() {
		String[] [] data1 = new String[1][3];
		data1[0][0]= "comp";
		data1[0][1]= "kaviya";
		data1[0][2]= "s";
		return data1;
	}
}
